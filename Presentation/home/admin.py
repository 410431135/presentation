from django.contrib import admin
from .models import *


@admin.register(Student)
class Student(admin.ModelAdmin):
	pass


@admin.register(Student_report)
class Student_report(admin.ModelAdmin):
	pass

@admin.register(Status)
class Status(admin.ModelAdmin):
	pass