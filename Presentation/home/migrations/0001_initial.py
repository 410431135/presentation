# Generated by Django 2.2.5 on 2021-11-04 02:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('sStudent_id', models.CharField(max_length=16, primary_key=True, serialize=False, verbose_name='學號')),
                ('sNickname', models.CharField(blank=True, max_length=128, null=True, verbose_name='暱稱')),
                ('sPassword', models.CharField(blank=True, max_length=32, null=True, verbose_name='密碼')),
                ('iNowScore', models.IntegerField(blank=True, default=0, null=True, verbose_name='給分')),
                ('sComment', models.CharField(blank=True, max_length=256, null=True, verbose_name='評語')),
                ('Account', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
