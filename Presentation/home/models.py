from django.db import models
from django.contrib.auth.models import User

class Student(models.Model):
	sStudent_id = models.CharField(verbose_name='學號', max_length=16, primary_key = True)
	sNickname = models.CharField(verbose_name='暱稱', max_length=128, null=True, blank=True)
	sPassword = models.CharField(verbose_name='密碼', max_length=32, null=True, blank=True)
	iNowScore = models.IntegerField(verbose_name='給分', default = 0, null=True, blank=True)
	sComment = models.CharField(verbose_name='評語', max_length=256, null=True, blank=True)

	Account = models.OneToOneField(User, unique=True ,on_delete = models.CASCADE)
	iTotalScore = models.FloatField(verbose_name='總得分',default = 0, null=True, blank=True)
	iTotalReport = models.IntegerField(verbose_name='報告次數', default = 0, null=True, blank=True)

class Student_report(models.Model):
	student = models.ForeignKey(User, on_delete=models.CASCADE)
	sDate = models.DateField()
	iScore = models.FloatField(verbose_name='得分', default = 0, null=True, blank=True)
	def __str__(self):
		return str(self.sDate)+':'+str(self.student.last_name)

	
class Status(models.Model):
	now_report = models.CharField(verbose_name='目前報告', max_length=128, null=True, blank=True)
	next_report = models.CharField(verbose_name='下一個報告報告', max_length=128, null=True, blank=True)
