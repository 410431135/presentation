# urls.py
from django.urls import path
from . import views
 
urlpatterns = [
    path('', views.home),
    path('set', views.setAccount),
    path('clear', views.clear),
    path('logout', views.logout_view),
    path('leaderboard', views.leaderboard),
    path('update', views.update),
    path('change', views.change_next_report),
]
