from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
import random
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
import datetime

def update(request):
	# 誰報告
	status = Status.objects.all()[0]
	s = status.now_report
	s = User.objects.filter(last_name = s)[0]

	# 當下的分數給某人 to student_report
	student_list = Student.objects.all()
	total = 0
	number = len(student_list)
	actual_num = 0
	for student in student_list:
		total += student.iNowScore
		if student.iNowScore != 0:
			actual_num += 1
	if (actual_num != 0):
		average = total/actual_num
	else:
		average = 0
	
	if average != 0:
		report = Student_report(student = s)
		report.iScore = average
		report.sDate = datetime.datetime.now()
		report.save()


	# 重置評分欄位
	student_list = Student.objects.all()
	for student in student_list:
		student.iNowScore = 0
		student.sComment = ''
		student.save()
	# 更新總分、報告次數

	reports = Student_report.objects.filter(student=s)
	totalScore = 0
	for student_report in reports:
		totalScore += student_report.iScore

	s.student.iTotalScore = totalScore
	s.student.iTotalReport = len(reports)
	s.student.save()

	# 更換下一組
	status.now_report = status.next_report
	choice = random.choice(student_list)
	status.next_report = choice.Account.last_name
	status.save()

	return redirect(home)

def leaderboard(request):
	student_list = User.objects.all().order_by('last_name')
	"""更新總分
	for s in student_list:
		if s.username=='kyle':
			continue
		reports = Student_report.objects.filter(student=s)
		totalScore = 0
		for student_report in reports:
			totalScore += student_report.iScore

		s.student.iTotalScore = totalScore
		s.student.iTotalReport = len(reports)
		s.student.save()
	"""


	return render(request, 'home/leaderboard.html', locals())

def logout_view(request):
	logout(request)
	return HttpResponse("logout sucessfully")

def home(request):
	user = request.user
	is_login = user.is_authenticated
	
	if request.POST != {}:
		sStudent_id = request.POST['sStudent_id']
		sPassword = request.POST['sPassword']
		sNickname = request.POST['sNickname']
		iNowScore = request.POST['iNowScore']
		sComment = request.POST['sComment']


		if sPassword != '':
			user = authenticate(request, username=sStudent_id, password=sPassword)
			if user is not None:
				login(request, user)
				is_login = user.is_authenticated
		if is_login and user.username!='kyle':
			student = user.student
		else:
			student = None

		if student != None:
			student.iNowScore = iNowScore
			if sNickname != '':
				student.sNickname = sNickname
			student.sComment = sComment
			student.save()
		else:
			status = "學號或密碼錯誤QQ"

		return redirect(home)

	student_list = Student.objects.all().order_by('-iNowScore')
	total = 0
	number = len(student_list)
	actual_num = 0
	for student in student_list:
		total += student.iNowScore
		if student.iNowScore != 0:
			actual_num += 1
	if (actual_num != 0):
		average = total/actual_num
	else:
		average = 0
	average = "%.2f" % (average)

	# 現在報告的人、下一個報告的人
	status = Status.objects.all()[0]
	now_report = status.now_report
	next_report = status.next_report
	show_button = False
	if user.username == 'kyle':
		show_button = True

	return render(request, 'home/home.html', locals())


def setAccount(request):
	# first_name = 班級, last_name=姓名
	lots = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUZWXYZ0123456789"
	nicknames = ['可愛的使用者','勾追的人', '荒野中的一匹狼', '煞氣a', 
					'暱稱五個字', '狠愛過', '森上 梅友前','窩4大正咩']
	status = False
	if request.POST != {}:
		if request.POST['sStudent_id'] != '':
			sStudent_id = request.POST['sStudent_id']
		else:
			return render(request, 'home/setAccount.html', locals())
		sPassword = ""
		for _ in range(4):
			sPassword += random.choice(lots)

		user = User()
		user.username = sStudent_id
		user.first_name = request.POST['sClass']
		user.last_name = request.POST['sName']
		user.set_password(sPassword)
		user.save()

		student = Student()
		student.sStudent_id = sStudent_id
		student.sPassword = sPassword
		student.sNickname = random.choice(nicknames)
		student.Account = user
		student.save()
		status = True

	student_list = Student.objects.all()


	return render(request, 'home/setAccount.html', locals())

def change_next_report(request):
	status = Status.objects.all()[0]
	student_list = Student.objects.all()
	choice = random.choice(student_list)
	status.next_report = choice.Account.last_name
	status.save()
	return redirect(home)

# no longer needed
def clear(request):
	student_list = Student.objects.all()
	for student in student_list:
		student.iNowScore = 0
		student.sComment = ''
		student.save()
	return redirect(home)

